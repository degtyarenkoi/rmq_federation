#!/usr/bin/env python
import pika
from optparse import OptionParser


"""
    parse() method
    returns a dictionary of parameters, inputted by user
"""
def parse():
    parser = OptionParser(usage='usage: %prog [options] username password')
    parser.add_option('-u', '--username',
        action='store',
        dest='username',
        help='Input username')
    parser.add_option('-p', '--password',
        action='store',
        dest='password',
        help='Input password')
    parser.add_option('-q', '--queue',
        action='store',
        dest='queue',
        default='fedqueue',
        help='Input queue name to send messages')
    parser.add_option('-n', '--node',
        action='store',
        dest='node',
        default='rmqapp1',
        help='Input node name')
    parser.add_option('-g', '--gateway',
        action='store',
        dest='gateway',
        default='5672',
        help='Input gateway(port)')
    parser.add_option('-v', '--vhost',
        action='store',
        dest='vhost',
        default='/',
        help='Input virtual host')
    options, args = parser.parse_args()

    if options.username == None:
        parser.error('Input username')
    elif options.password == None:
        parser.error('Input password')
    else:
        return options
    

"""
    main() method
    iplements main logic of consuming data from RMQ
"""
def main():
    options = parse()
    print options
    credentials = pika.PlainCredentials(options.username, options.password)
    params = pika.ConnectionParameters(options.node,
                                       int(options.gateway),
                                       options.vhost,
                                       credentials)
    connect = pika.BlockingConnection(params)
    channel = connect.channel()


    def callback(ch, method, properties, body):
        print('Received %r' % body)


    channel.basic_qos(prefetch_count=10)
    channel.basic_consume(callback, queue=options.queue)
    print('waiting for messages')
    channel.start_consuming()


if __name__ == "__main__":
    main()
