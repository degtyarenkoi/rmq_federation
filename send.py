#!/usr/bin/env python
import pika
from optparse import OptionParser
import os, time
import tempfile
import logging


"""
    parse() method 
    returns a dictionary of parameters, inputted by user
"""
def parse():
    parser = OptionParser(usage="usage: %prog [options] username password filename")
    parser.add_option("-u", "--username",
        action="store",
        dest="username",
        help="input username")
    parser.add_option("-p", "--passwd",
        action="store",
        dest="password",
        help="input password")
    parser.add_option("-f", "--file",
        action="store",
        dest="filename",
        help="select file: %r" % os.listdir("/root"))
    parser.add_option("-q", "--queue",
        action="store",
        dest="queue",
        default="fedqueue",
        help="select queue name to send messages")
    parser.add_option("-g", "--gateway",
        action="store",
        dest="gateway",
        default="5672",
        help="select gateway(port)")
    parser.add_option("-v", "--vhost",
        action="store",
        dest="vhost",
        default="/",
        help="select a virtual host")
    parser.add_option("-n", "--node",
        action="store",
        dest="node",
        default="rmqapp1",
        help="select a node to send messages")
    options, args = parser.parse_args()
    
    if options.username == None:
        parser.error("Input username")
    elif options.password == None:
        parser.error("Input password")
    elif options.filename == None:
        parser.error("Input name of the file")
    else:
        return options


"""
    send_mess() method
    takes 7 parameters to connect and send data to RMQ,
    saves data in it's own file system using temporary files
"""
def send_mess(username, password, filename, gateway, vhost, node, queue):
    credentials = pika.PlainCredentials(username, password)
    params = pika.ConnectionParameters(node, 
                                       int(gateway), 
                                       vhost, 
                                       credentials)
    connect = pika.BlockingConnection(params)
    channel = connect.channel()
    channel.queue_declare(queue, durable=True)

    try:
        file = open(filename, "r")
        file_content = file.read()
        channel.basic_publish(exchange='', routing_key=queue, body=file_content)
        logging.info(u'file ' + filename + ' was successfully delivered')  
        print('sent a file %r' % filename)
#        os.remove(filename)   
    except IOError:
        print("ERROR: file doesnt exists")
    except pika.exceptions.AMQPConnectionError:
        logging.warning(u'disconnected from RMQ')
    except pika.exceptions.ConnectionClosed:
        logging.error(u'connection was closed, data werent delivered')
    finally:
        connect.close()


'''
    create_temp_file() method
    realise creation temporary files out of new files in directory
'''
def create_temp_file():
    path_to_watch = '.'
    before = list([f for f in  os.listdir(path_to_watch)])
    
    while True:
        time.sleep(1)
        after = list([f for f in  os.listdir(path_to_watch)])
        added = [f for f in after if not f in before]

        if added:
            for f in added:
                new_file = open(f, "r")
                file_content = new_file.read()
                print file_content
                with tempfile.NamedTemporaryFile() as tmp_file:
                    tmp_file.write(file_content)
                    logging.info(u'content of file successfully saved to temporary file ' + tmp_file.name)
        before = after


'''
    daemonize() method
    helps create temporary files out of new files in cron mode
'''
def daemonize():
    pid  = os .fork()
    if pid == 0:
        os.setsid()
        pid = os.fork()
        if pid == 0:
            print('DAEMON STARTED')
            create_temp_file()
        else:
            os._exit(0)
    else:
        os._exit(0)


"""
    main() method
    implements main logic of getting data from user and send a file to RMQ
"""
def main():
    logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', 
        level=logging.DEBUG, filename=u'logs.log')

    options = parse()
    username = options.username
    password = options.password
    filename = options.filename
    gateway = options.gateway
    vhost = options.vhost
    node = options.node
    queue = options.queue

    send_mess(username, password, filename, gateway, vhost, node, queue)


if __name__ == "__main__":
    main()
    daemonize()
